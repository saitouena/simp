import Control.Monad.State
import qualified Data.Map as Map
import Control.Monad.Trans.Except
import Test.HUnit hiding (State)
import System.IO

data SimpExp = SInt Int
             | SVar String
             | SAssign String SimpExp
             | SSeq [SimpExp]
             | SIf SimpExp SimpExp SimpExp
             | SWhile SimpExp SimpExp
             | SPlus SimpExp SimpExp
             | SMul SimpExp SimpExp
             | SMinus SimpExp SimpExp
             | SDiv SimpExp SimpExp
             | SEQ SimpExp SimpExp
             | SGT SimpExp SimpExp
             | SLT SimpExp SimpExp
             | SNot SimpExp
             deriving (Show,Eq)

data SimpVal = IntVal Int
             | Void
             deriving (Show,Eq)

type Env = Map.Map String SimpVal

type Eval = ExceptT String (State Env) SimpVal

runEval :: SimpExp -> (Either String SimpVal, Env)
runEval e = (runState $ runExceptT $ eval e) emptyEnv

emptyEnv :: Env
emptyEnv = Map.empty

data Op2 = Plus | Mul | Minus | Div | GTOP | EQOP | LTOP
data Op1 = Not

eval :: SimpExp -> Eval
eval e = do
  case e of
    SInt n -> return $ IntVal n
    SPlus e1 e2 -> eval2Op e1 e2 Plus
    SMul e1 e2 -> eval2Op e1 e2 Mul
    SMinus e1 e2 -> eval2Op e1 e2 Minus
    SDiv e1 e2 -> eval2Op e1 e2 Div
    SEQ e1 e2 -> eval2Op e1 e2 EQOP
    SGT e1 e2 -> eval2Op e1 e2 GTOP
    SLT e1 e2 -> eval2Op e1 e2 LTOP
    SNot e -> eval1Op e Not
    SVar s -> evalVar s
    SAssign s e -> evalAssign s e
    SSeq es -> evalSeq es
    SIf p e1 e2 -> evalIf p e1 e2
    SWhile p e -> evalWhile p e

eval2Op :: SimpExp -> SimpExp -> Op2 -> Eval
eval2Op e1 e2 op = do
  IntVal n1 <- eval e1
  IntVal n2 <- eval e2
  case op of
    Plus -> return $ IntVal (n1+n2)
    Mul -> return $ IntVal (n1*n2)
    Minus -> return $ IntVal (n1-n2)
    Div -> case n2 of
             0 -> throwE "zero division."
             _ -> return $ IntVal (n1`div`n2)
    EQOP -> return $ if n1 == n2
                     then IntVal 1
                     else IntVal 0
    GTOP -> return $ if n1 < n2
                     then IntVal 1
                     else IntVal 0
    LTOP -> return $ if n1 > n2
                     then IntVal 1
                     else IntVal 0

eval1Op :: SimpExp -> Op1 -> Eval
eval1Op e op = case op of
                 Not -> evalNot e

evalNot :: SimpExp -> Eval
evalNot e = do
  v <- eval e
  case v of -- ここで eval e とするとだめ do notationのちからで剥がさないと...?
    IntVal 0 -> return $ IntVal 1
    Void -> return $ IntVal 1
    IntVal _ -> return $ IntVal 0

evalVar :: String -> Eval
evalVar s = do
  env <- get
  case Map.lookup s env of
    Nothing -> throwE "unbound variable"
    Just n -> return n
  
evalAssign :: String -> SimpExp -> Eval
evalAssign s e = do
  v <- eval e
  env <- get
  let newEnv = Map.insert s v env
  put newEnv
  return Void

evalSeq :: [SimpExp] -> Eval
evalSeq [] = throwE "no expression : SSeq"
evalSeq [e] = eval e
evalSeq (e:es) = eval e >> evalSeq es

evalIf :: SimpExp -> SimpExp -> SimpExp -> Eval
evalIf p c1 c2 = do
  pv <- eval p
  case pv of
    IntVal 0 -> eval c2
    Void -> eval c2
    _ -> eval c1

evalWhile :: SimpExp -> SimpExp -> Eval
evalWhile p e = do
  pv <- eval p
  case pv of
    IntVal 0 -> return $ Void
    Void -> return $ Void
    IntVal _ -> eval e >> evalWhile p e

prog1 = SInt 10 -- 10
prog2 = SPlus (SInt 10) (SInt 20) -- 30
prog3 = SMul prog1 prog2 -- 300
prog4 = SDiv prog3 (SInt 100) -- 3
prog5 = SMinus prog4 (SInt 100) -- -97
prog6 =
  SSeq [(SAssign "x" (SInt 100)),
        (SAssign "y" (SInt 10)),
        (SMul (SVar "x") (SVar "y"))]
prog7 =
  SSeq [(SAssign "n" (SInt 10)),
        (SAssign "acc" (SInt 1)),
        (SWhile
         (SVar "n")
         (SSeq [
             (SAssign "acc"
               (SMul (SVar "n") (SVar "acc"))),
             (SAssign "n"
              (SMinus (SVar "n") (SInt 1)))
             ])),
        (SVar "acc")]
prog8 =
  SSeq [(SAssign "n" (SInt 100)),
        (SAssign "x" (SInt 0)),
        (SWhile
         (SVar "n")
         (SSeq [
             (SAssign "x" (SPlus (SVar "x") (SVar "n"))),
             (SAssign "n" (SMinus (SVar "n") (SInt 1)))])),
        (SVar "x")]
prog9 = SDiv (SInt 10) (SInt 0)
prog10 = SVar "x"

getTestRes = fst . runEval

tests = TestList
  [
    "prog1" ~: getTestRes prog1 ~?= Right (IntVal 10),
    "prog2" ~: getTestRes prog2 ~?= Right (IntVal 30),
    "prog3" ~: getTestRes prog3 ~?= Right (IntVal 300),
    "prog4" ~: getTestRes prog4 ~?= Right (IntVal 3),
    "prog5" ~: getTestRes prog5 ~?= Right (IntVal (-97)),
    "prog6" ~: getTestRes prog6 ~?= Right (IntVal 1000),
    "prog7" ~: getTestRes prog7 ~?= Right (IntVal 3628800),
    "prog8" ~: getTestRes prog8 ~?= Right (IntVal 5050),
    "prog9" ~: getTestRes prog9 ~?= Left "zero division.",
    "prog10" ~: getTestRes prog10 ~?= Left "unbound variable"
  ]

main = do
  runTestText (putTextToHandle stderr False) tests
